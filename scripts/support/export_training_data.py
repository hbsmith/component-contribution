"""A script for writing the training data as a CSV file."""
# The MIT License (MIT)
#
# Copyright (c) 2013 The Weizmann Institute of Science.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich, Switzerland.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import argparse
import logging

from component_contribution.training_data import (
    FullTrainingData,
    ToyTrainingData,
)


logger = logging.getLogger("training data exporter")

if __name__ == "__main__":
    logger.setLevel(logging.INFO)
    parser = argparse.ArgumentParser(
        description="Prepare all thermodynamic "
        "training data in a .mat file for "
        "running component contribution."
    )
    parser.add_argument(
        "-t",
        action="store_true",
        dest="toy",
        help="use the toy example rather than the full " "database",
    )
    parser.add_argument(
        "outfile",
        type=argparse.FileType("w"),
        help="the path to the .csv file that should be "
        "written containing the training data",
    )

    args = parser.parse_args()
    if args.toy:
        logger.info("Gathering toy data for export")
        td = ToyTrainingData()
    else:
        logger.info("Gathering full data for export")
        td = FullTrainingData()
    td.reaction_df.to_csv(args.outfile)
