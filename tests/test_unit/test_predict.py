"""unit test for component-contribution predictions."""
# The MIT License (MIT)
#
# Copyright (c) 2013 The Weizmann Institute of Science.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich, Switzerland.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import pytest
from equilibrator_cache import Q_, Compound

from component_contribution.predict import GibbsEnergyPredictor


@pytest.fixture(scope="module")
def atp_compound(ccache) -> Compound:
    """Create ATP Compound."""
    return ccache.get_compound("KEGG:C00002")


def test_unresolved_reactions(reaction_dict, comp_contribution):
    """Test that the reaction cannot be resolved by CC."""
    unresolved_reactions = [reaction_dict[f"unresolved_{i}"] for i in range(3)]
    standard_dg, dg_cov = comp_contribution.standard_dg_multi(
        unresolved_reactions
    )
    dg_cov_unitless = dg_cov.m_as("kJ**2/mol**2")
    assert dg_cov_unitless[0, 0] == pytest.approx(5e10, rel=0.01)
    assert dg_cov_unitless[1, 1] == pytest.approx(2e10, rel=0.01)
    assert dg_cov_unitless[2, 2] == pytest.approx(2e10, rel=0.01)
    assert dg_cov_unitless[2, 1] == pytest.approx(2e10, rel=0.01)
    assert dg_cov_unitless[1, 2] == pytest.approx(2e10, rel=0.01)


@pytest.mark.parametrize(
    "p_h, ionic_strength, temperature, exp_standard_dg_prime, "
    "exp_sigma, uses_gc, reaction_name",
    [
        (6.0, 0.25, 298.15, -214.0, 6.7, False, "fermentation"),
        (7.0, 0.10, 298.15, -214.0, 6.7, False, "fermentation"),
        (8.0, 0.50, 298.15, -214.0, 6.7, False, "fermentation"),
        (6.0, 0.25, 298.15, -23.4, 0.3, False, "atpase"),
        (7.0, 0.25, 298.15, -25.8, 0.3, False, "atpase"),
        (8.0, 0.25, 298.15, -30.7, 0.3, False, "atpase"),
        (6.0, 0.25, 298.15, 4.8, 0.2, False, "transadenylate"),
        (7.0, 0.25, 298.15, 1.8, 0.2, False, "transadenylate"),
        (8.0, 0.25, 298.15, 0.9, 0.2, False, "transadenylate"),
        (6.0, 0.25, 298.15, 28.1, 1.0, True, "gc_0"),
        (7.0, 0.25, 298.15, 20.4, 1.0, True, "gc_0"),
        (8.0, 0.25, 298.15, 14.5, 1.0, True, "gc_0"),
        (6.0, 0.25, 298.15, -37.6, 3.1, True, "gc_1"),
        (7.0, 0.25, 298.15, -44.0, 3.1, True, "gc_1"),
        (8.0, 0.25, 298.15, -49.8, 3.1, True, "gc_1"),
    ],
)
def test_standard_dg_prime_calculation(
    p_h: float,
    ionic_strength: float,
    temperature: float,
    exp_standard_dg_prime: float,
    exp_sigma: float,
    uses_gc: bool,
    reaction_name: str,
    reaction_dict: dict,
    comp_contribution: GibbsEnergyPredictor,
):
    """Test the standard dG' of transadenylate in different pHs."""
    reaction = reaction_dict[reaction_name]

    assert reaction.is_balanced()

    assert comp_contribution.is_using_group_contribution(reaction) == uses_gc

    standard_dg_prime = comp_contribution.standard_dg_prime(
        reaction,
        p_h=Q_(p_h),
        ionic_strength=Q_(ionic_strength, "M"),
        temperature=Q_(temperature, "K"),
    )
    assert standard_dg_prime.value.m_as("kJ/mol") == pytest.approx(
        exp_standard_dg_prime, abs=0.1
    )
    assert standard_dg_prime.error.m_as("kJ/mol") == pytest.approx(
        exp_sigma, abs=0.1
    )


@pytest.mark.parametrize(
    "p_h, ionic_strength, temperature, exp_standard_dg_prime, exp_sigma",
    [
        (6.0, 0.25, 298.15, -2367.1, 1.5),
        (7.0, 0.25, 298.15, -2296.4, 1.5),
        (8.0, 0.25, 298.15, -2227.5, 1.5),
    ],
)
def test_standard_dg_formation(
    p_h,
    ionic_strength,
    temperature,
    exp_standard_dg_prime,
    exp_sigma,
    atp_compound,
    comp_contribution,
):
    """Test the standard dG' of formation of ATP."""
    standard_dgf = comp_contribution.standard_dgf_prime(
        atp_compound,
        p_h=Q_(p_h),
        ionic_strength=Q_(ionic_strength, "M"),
        temperature=Q_(temperature, "K"),
    )
    assert standard_dgf.value.m_as("kJ/mol") == pytest.approx(
        exp_standard_dg_prime, abs=0.1
    )
    assert standard_dgf.error.m_as("kJ/mol") == pytest.approx(
        exp_sigma, abs=0.1
    )


def test_dg_analysis(reaction_dict, comp_contribution):
    """Test the standard dG' of formation of ATP."""
    reaction = reaction_dict["atpase"]
    analysis = comp_contribution.dg_analysis(reaction)

    assert analysis[0]["index"] == 595
    assert analysis[0]["w_rc"] == pytest.approx(0.381, abs=1e-3)
    assert analysis[0]["w_gc"] == pytest.approx(0.0, abs=1e-3)
