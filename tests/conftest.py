# The MIT License (MIT)
#
# Copyright (c) 2019 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
# Copyright (c) 2019 Institute for Molecular Systems Biology,
# ETH Zurich, Switzerland.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


"""Provide fixtures for the whole test suite."""


from typing import Dict

import pytest
from equilibrator_cache import (
    Q_,
    CompoundCache,
    Reaction,
    create_compound_cache_from_quilt,
)

from component_contribution.parameters import CCModelParameters
from component_contribution.predict import GibbsEnergyPredictor
from component_contribution.training_data import ToyTrainingData, TrainingData


@pytest.fixture(scope="session")
def ccache() -> CompoundCache:
    """Create a compound cache for testing."""
    return create_compound_cache_from_quilt(version="0.2.7")


@pytest.fixture(scope="module")
def comp_contribution() -> GibbsEnergyPredictor:
    """Create GibbsEnergyPredictor object."""
    params = CCModelParameters.from_quilt(version="0.2.18")
    return GibbsEnergyPredictor(params)


@pytest.fixture(scope="module")
def toy_training_data(ccache: CompoundCache) -> TrainingData:
    """Create a ToyTrainingData object."""
    return ToyTrainingData(ccache=ccache, override_p_mg=Q_(14))


@pytest.fixture(scope="module")
def toy_training_data_mg(ccache: CompoundCache) -> TrainingData:
    """Create a ToyTrainingData object."""
    return ToyTrainingData(ccache=ccache)


@pytest.fixture(scope="module")
def reaction_dict(ccache) -> Dict[str, Reaction]:
    """Create a ATP hydrolysis reaction."""
    formulas = [
        ("fermentation", "kegg:C00031 = 2 kegg:C00469 + 2 kegg:C00011"),
        ("atpase", "kegg:C00002 + kegg:C00001 = kegg:C00008 + kegg:C00009"),
        ("hexokinase", "kegg:C00002 + kegg:C00031 = kegg:C00008 + kegg:C00092"),
        ("transadenylate", "2 kegg:C00008 = kegg:C00002 + kegg:C00020"),
        ("gc_0", "kegg:C05576 + kegg:C00003 = kegg:C05577 + kegg:C00004"),
        ("gc_1", "kegg:C16595 + kegg:C00003 = kegg:C16596 + kegg:C00004"),
        (
            "adenosine_kinase",
            "kegg:C00020 + kegg:C00001 = kegg:C00212 + kegg:C00009",
        ),
        (
            "unresolved_0",
            "kegg:C09844 + kegg:C00003 + kegg:C00001 = "
            "kegg:C03092 + kegg:C00004",
        ),
        (
            "unresolved_1",
            "kegg:C00028 + 2 kegg:C00533 + 2 kegg:C00001 = "
            "kegg:C00030 + 2 kegg:C00088",
        ),
        (
            "unresolved_2",
            "kegg:C00028 + 2 kegg:C00004 = kegg:C00030 + 2 kegg:C00003",
        ),
    ]
    return {
        name: Reaction.parse_formula(ccache.get_compound, formula)
        for name, formula in formulas
    }
